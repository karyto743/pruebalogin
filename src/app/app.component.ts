import { Component } from '@angular/core';
//import {Login} from './modelos/login';
//import {LoginService} from './login/login.service';
@Component({
  selector: 'app-root',
  template: `
  <router-outlet></router-outlet>
  `
})
export class AppComponent {
  constructor(){}
}
