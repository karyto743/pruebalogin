import { Injectable } from '@angular/core';
import { HttpClient, HttpHeaders } from '@angular/common/http';
import { Login } from '../modelos/login';
//import { MsjError } from './modelos/respuesta_error';
//import { Observable} from 'rxjs';
import { catchError, retry } from 'rxjs/operators'
import { HandleError } from '../comun/handleError.service';
import { Observable, Subject } from 'rxjs';
@Injectable({
  providedIn: 'root'
})
export class LoginService {
  constructor(private http: HttpClient, private handleError:HandleError) { 
  }
  httpOptions = {
    headers: new HttpHeaders({
      'Content-Type': 'application/json'
    })
  }
  //:Observable<MsjError>
  iniciarSesion(datos:Login){
    return this.http.post('http://159.89.50.39:5000/auth/perfil/login',datos,this.httpOptions)
    .pipe(  
      catchError(this.handleError.handleError)
    )
  }

}
