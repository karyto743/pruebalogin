import { HttpErrorResponse } from '@angular/common/http';
import { throwError } from 'rxjs';
import { ErrorHandler, Injectable } from '@angular/core';

@Injectable()
export class HandleError implements ErrorHandler {
    handleError(error: HttpErrorResponse) {
        /*if (error.error instanceof ErrorEvent) {
          // A client-side or network error occurred. Handle it accordingly.
          console.error('Ocurrió un error:', error.error.message);
        } else {
          // The backend returned an unsuccessful response code.
          // The response body may contain clues as to what went wrong,
          console.error(              
            `Código devuelto por el servidor ${error.status}, ` +
            `mensaje: ${error.error.message}`);
        }*/
        // return an observable with a user-facing error message
        return throwError(error.error.message)
        /*return throwError(
          'Algo salio mal; Por favor, inténtelo de nuevo más tarde.');*/
    };
}