import { Component } from '@angular/core';
//import {Login} from './modelos/login';
//import {LoginService} from './login/login.service';
@Component({
  selector: 'user-component',
  //templateUrl: './app.component.html',
  //styleUrls: ['./app.component.css']
  template: `
  <router-outlet></router-outlet>
  `
})
export class UserComponent {
  constructor(){}
}
