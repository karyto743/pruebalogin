import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { VierUserComponent } from './view-user/view.component';
import { UserComponent } from './user.component';
import {UserRoutingModule} from './user-routing.module';
import { LoginService } from '../login/login.service';

@NgModule({
  imports: [
    CommonModule,
    UserRoutingModule,
    //RouterModule,
    //RouterModule.forChild(UserRoutes),
  ],
  declarations: [
    VierUserComponent,
    UserComponent
  ],
  providers:[
    LoginService
  ]
})
export class UserModule { }