
import { Routes,RouterModule } from '@angular/router';
import { VierUserComponent } from './view-user/view.component';
import { UserComponent } from './user.component';
import { NgModule } from '@angular/core';

const userRoutes: Routes = [
  //{ path: '', redirectTo: '/perfil' },
  //{ path: 'perfil', component: VierUserComponent },
  //{ path: '**', component: PageNotFoundComponent }
  {
    path: '',
    component: UserComponent,
    children: [
      {
        path: '',
        children: [
          { path: 'perfil', component: VierUserComponent },
          { path: '', component: VierUserComponent }
        ]
      }
    ]
  }

];

@NgModule({
  imports: [
    RouterModule.forChild(userRoutes)
  ],
  exports: [
    RouterModule
  ]
})
export class UserRoutingModule { }