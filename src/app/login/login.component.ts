import { Component } from '@angular/core';
import {Login} from '../modelos/login';
import {LoginService} from '../login/login.service';
import { Router } from '@angular/router';
import { Subscription } from 'rxjs';
@Component({
  selector: 'app-login',
  templateUrl: './login.component.html',
  styleUrls: ['./login.component.css']
})
export class LoginComponent {
  constructor(private loginService: LoginService, public router: Router) {}
  login:Login={
    nombre_contacto:"",
    password:""
  }
  mensaje:string;
  ingresar(){
    this.mensaje="";    
    this.loginService.iniciarSesion(this.login)
    .subscribe(res=>{      
      this.router.navigate(['/admin']);
    },error=>{
      console.log("TENGO UN ERROR", error);
      this.mensaje=error;
    })
  }

}